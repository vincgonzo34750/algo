package silo_algo1;

import java.sql.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import static java.lang.System.currentTimeMillis;

public class Main {

    public static void main(String[] args) {

        List<Integer> test = new ArrayList(Arrays.asList( 5, 364, 80, 99, 4, 4, 22, 54, 22, 746, 4 ));

//        displayArr(test);
//        suppAtPos(test, 2);
//        displayArr(test);
//        suppAllVal(test, 5);
//        displayArr(test);
//        suppAllVal(test, 4);
//        displayArr(test);
        System.out.println(currentTimeMillis());;
        List<Integer> ll = randList(10, 50, 5);
        displayArr(ll);
        System.out.println(currentTimeMillis());
        System.out.println(currentTimeMillis());;
        List<Integer> l1 = randList(25, 50, 5);
        displayArr(l1);
        System.out.println(currentTimeMillis());
//
//        displayArr(l1);
//        System.out.println(currentTimeMillis());
//        List<Integer> l2 = randList(40000, 50, 5);
//        System.out.println(currentTimeMillis());
//        displayArr(l2);
//        System.out.println(currentTimeMillis());
//        List<Integer> l3 = randList(1000000, 50, 5);
//        System.out.println(currentTimeMillis());
        //displayArr(l3);
        System.out.println(currentTimeMillis());
    }

    public static void displayArr(List<Integer> a){
        for (int i=0; i < a.size(); i++){
            System.out.print(" " + a.get(i) );
        }
        System.out.println();
    }

    public static void suppAtPos(List<Integer> a, int pos){
        for (int w = pos; w < a.size() - 1; w++){
            a.set(w, a.get(w + 1));
        }
        reduceTo(a, a.size() - 1);
    }
//
//    5 364 80 99 4 4 22 54 22 746 4
//
//  w 0  1   2  3  3 3 4 5  6   7  7
//  j 0  1  2  3  4 5  6 7  8  9 10
    public static void suppAllVal(List<Integer> l, int val){
        int w = 0;
        for(int j = 0; j < l.size(); ++j){
            if(l.get(j) != val){
                l.set(w, l.get(j));
                ++w;
            };
        }
        reduceTo(l, w);
    }
//
//    Ecrivez une fonction qui génère une liste aléatoire de dimension choisie
//    dans laquelle environ p pourcent des éléments ont une valeur choisie :
//    public static List<Integer> randList(int size, int p, int val );
//    p peut prendre des valeur de 0 à 100.
//    Par exemple un appel randList(10, 50, 5) pourrait donner la liste cidessus.
//            4. Ecrivez un programme qui génère des listes de longueur 10.000, 20.000,
//            40.000 et 80.000, et dont environ 50% des valeurs sont choisies.
//    Supprimez cette valeur des listes. Pour chaque cas, chronométrez et
//    notez le temps mis par votre fonction. En Java, vous pouvez utiliser
//java.lang.System.currentTimeMillis()
//        5. Comment le temps change-t-il quand la taille de la liste double ?
//            6. Si le temps est multiplié par 4 quand la taille de vos listes double, pouvezvous trouver un meilleur algorithme de suppression

    // p de 0 a 100
    public static List<Integer> randList(int size, int p, int val){
        List<Integer> list = new ArrayList<>();
//        int nbVal = size * p / 100;
        Random r = new Random();
        for (int i = 0; i < size; i++){
//            if(r.nextInt(size) <= p && nbVal != 0){
            if(r.nextInt(size) <= p){
                list.add(val);
            }else
                list.add(r.nextInt(size));
        }

        return list;
    }

    public static <E> void reduceTo(List<E> ls, int newSize) {
        if(newSize < ls.size())
            ls.subList(newSize, ls.size()).clear();
    }

    public static void displayK(int[] z){
        System.out.println();
        for (int t : z)
            System.out.print(t + " ");
    }
}
