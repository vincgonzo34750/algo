package silo_algo1;

public class Recursive_max {

    public static void main(String[] args) {
        int[] ls = {-5, -2};
        int[] lsE = {5, 2, 7, 33, 2, 6, 99, 3};

        int maxLs = recursiveMaxWC(ls);
        System.out.println(maxLs);
        int maxLs2 = recursiveMaxWC(lsE);
        System.out.println(maxLs2);
    }

    // 5, 2, 7, 33, 2, 6, 99, 3
    // 5 : 2, 7, 33, 2, 6, 99, 3
    // 5 : 7, 33, 2, 6, 99, 3
    // 7 : 33, 2, 6, 99, 3
    // 33 : 2, 6, 99, 3
    // 33 : 6, 99, 3
    // 33 : 99, 3
    // 99 : 3
    // 99 :
    public static int recursiveMax(int[] ls, int i, int i1){
        if(ls.length == 0) {
            return 0;
        }else
            return recursiveMax(ls, 0);
    }
    public static int recursiveMax(int[] ls, int index){
        int max = ls[index];
        if(existsIndex(ls,index + 1)) {
            if (ls[index] > ls[index + 1]){
                ls[index + 1] = ls[index];
            }
            return recursiveMax(ls, index+1);
        }
        return max;
    }

    public static int recursiveMaxWC(int[] ls){
        if(ls.length == 0) {
            return 0;
        }else
            return recursiveMaxWC(ls, 0, 0);
    }
    public static int recursiveMaxWC(int[] ls, int index, int compare){
        //TODO refacto this !!!!
        int max = (existsIndex(ls,index + 1) && ls[index] > ls[index + 1]) ? ls[index] : ls[index + 1];
        compare = (max > compare)? max : compare;
        if(existsIndex(ls, index + 1)) {
            return recursiveMax(ls, (index + 1), compare);
        }
        return compare;
    }


    public static boolean existsIndex(int[] ls, int size){
        return ls.length > size;
    }
}
