package silo_algo1;

public class GetKValueForIdx {

    public static void main(String[] args) {
        int[] v = {-9,-6,-3,0,1,2,6,8,12};
        int[] b = {-200,-120,-9,-6,-3,0,1,7,8,10,11};
        int[] c = {-5000,-200,-120,-9,-6,-3,0,1,2,6,8,12};
        int[] z = {-9,-6,-3,-2,0,1,2,6,8,12};
        int[] m = {-18,-17,-16,-13,-12,-11,-2,0,1,2,3,4,13};

        System.out.println("le tableau ordonné suivant à bien une valeur k egal à son indice dans le tab ::" + searchIdxKEqK(v));
//        System.out.println("index positif de tab v ::" + posIdx(v));
        System.out.println("le tableau ordonné suivant à bien une valeur k egal à son indice dans le tab ::" + searchIdxKEqK(b));
//        System.out.println("index positif de tab b ::" + posIdx(b));
        System.out.println("le tableau ordonné suivant à bien une valeur k egal à son indice dans le tab ::" + searchIdxKEqK(c));
//        System.out.println("index positif de tab c ::" + posIdx(c));
        System.out.println("le tableau ordonné suivant à bien une valeur k egal à son indice dans le tab ::" + searchIdxKEqK(z));
        System.out.println("index positif de tab z ::" + posIdx(m));
        System.out.println("le tableau ordonné suivant à bien une valeur k egal à son indice dans le tab ::" + searchIdxKEqK(m));
//        System.out.println("index positif de tab z ::" + posIdx(z));
    }

    private static int posIdx(int[] v ){ // recherche dichotomique log(N)
        return getPosIndex(v, 0, v.length);
    }

    private static int getPosIndex(int[] v, int deb, int fin){
        if(deb == fin) return deb;
        int mil = (deb + fin) / 2;
        if(v[mil] > 0) return getPosIndex(v, deb, mil);
        return getPosIndex(v, mil + 1, fin);
    }

    private static boolean searchIdxKEqK(int[] tab){ // recherche correspondance v[k] == k log(N) ?
        return getKVal(tab, posIdx(tab), tab.length);
    }


    private static boolean getKVal(int[] v, int deb, int fin){
        if(deb == fin && v[deb] != deb) return false;
        if(deb == fin && v[deb] == deb) return true;
        int mil = (deb + fin) / 2;
        if(v[mil] == mil) return true;
        if(v[mil] < mil) return getKVal(v, mil + 1, fin);
        return getKVal(v, deb, mil);
    } //V1
}
