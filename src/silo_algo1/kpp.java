package silo_algo1;

public class kpp  {

    public static void main(String[] args) {
        int[] test = {25, 4, 1, 5, 71, 63, 660, 88, 2, 99, 135, 76, 3, 80};
        displayArray(test);

        System.out.println("le " + 2 + "eme plus petit element est ::: " + getKPP(2, test)); // attendu 2
        System.out.println("======== After first kpp search ");
        displayArray(test);
        System.out.println("le " + 9 + "eme plus petit element est ::: " + getKPP(9, test)); // attendu 76
        System.out.println("======== After second kpp search ");
        displayArray(test);
    }

    static void displayArray(int[] tab){
        System.out.println("====================");
        for (int i : tab ) {
            System.out.print(i + " ");
        }
        System.out.println();
    }

    static int partition(int[] tab, int deb, int fin) {
        int pivot = tab[deb]; // On choisit le 1er
        int pos = deb + 1;
        while(pos < fin)
            if((tab[pos] - pivot) >= 0)
                swap(tab, pos, --fin);
            else
                ++pos;
        swap(tab, deb, --pos);
        return pos;
    }
    static void myKpp(int[] tab, int k) {
        int pos = partition(tab, 0, tab.length);
        System.out.println("PIVOT :: " + pos);
        if(k < pos)
            sort(tab, 0, pos - 1);
        else
            sort(tab, pos, tab.length);
    }

    static void sort(int[] tab, int deb, int fin) {
        if(fin - deb >= 2){
            int pos = partition(tab, deb, fin);
            sort(tab, deb, pos);
            sort(tab, pos + 1, fin);
        }
    }

    static int getKPP(int k, int[] tab){
        if(k > 0 && k < tab.length){
            myKpp(tab, k);

            return tab[k - 1];
        }
        return 0;
    }
    static void swap(int[] tab, int a, int b) {
        int t = tab[a];
        tab[a] = tab[b];
        tab[b] = t;
    }

}

