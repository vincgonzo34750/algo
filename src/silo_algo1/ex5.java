package silo_algo1;

import static silo_algo1.Main.displayK;

public class ex5 {

    public static void main(String[] args) {
        int[] w = {0, -7, 0, 0, 5, 9, -4, 0, -2, -9, -4, -8, 2};

        displayK(w);
        reorderArray(w);
        displayK(w);
    }

    public static int[] reorderArray(int[] w){
        int idxInconnu = 0, idxNeg = 0, idxPos = w.length - 1;
        /**
         *      -7  -8 0  0  0                         2  5
         *      0  -7  0  0  5  9  -4  0  -2  -9  -4  -8  2
         * idxI 1  2   3  4  5
         * idxN 0  1  1  1   2
         * idxP 12 12 12 12 10
         */
        while(idxInconnu <= idxPos){
            int tmp;
            if(w[idxInconnu] == 0)
                ++idxInconnu;
            if(w[idxInconnu] < 0){
                tmp = w[idxNeg];
                w[idxNeg] = w[idxInconnu];
                w[idxInconnu] = tmp;
                ++idxNeg;++idxInconnu;
            }
            if(w[idxInconnu] > 0){
                tmp = w[idxPos];
                w[idxPos] = w[idxInconnu];
                w[idxInconnu] = tmp;
                --idxPos;
            }
        };

        return w;
    }
}
