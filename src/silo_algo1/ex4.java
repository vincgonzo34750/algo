package silo_algo1;

import static silo_algo1.Main.displayK;

public class ex4 {
    //    Ecrivez une fonction qui regroupe les éléments d’un tableau composé
    //    exclusivement des entiers -1, 0 et 1 de façon à ce que tous les -1 soient
    //    au début, suivis des 0 et, finalement des 1. Par exemple, le tableau
    //[0, -1, 0, 0, 1, 1, -1, 0, -1, -1, -1, -1, 1]
    //    donnerait
    //[-1, -1, -1, -1, -1, -1, 0, 0, 0, 0, 1, 1, 1]
    //    Bien sûr, trier le tableau donne le résultat. Mais est-ce vraiment
    //    nécessaire de trier ?
    public static void main(String[] args) {
        int[] j = {0, -1, 0, 0, 1, 1, -1, 0, -1, -1, -1, -1, 1};

        displayK(j);
        allBrandArray(j);
        displayK(j);
    }



    public static int[] allBrandArray(int[] k){ // T(O)
        int a = 0, b = 0, c = 0;
        for (int w = 0; w < k.length;w++){
            if(k[w] == -1) a++;
            else if(k[w] == 0) b++;
            else c++;
        }
        for(int w = 0; w < k.length;w++)
            if(a > 0){
                k[w] = -1;--a;
            }else if(b > 0){
                k[w] = 0; --b;
            }else k[w] = 1;
        return k;
    }
}
