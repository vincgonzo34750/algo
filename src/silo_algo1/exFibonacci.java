package silo_algo1;


import static java.lang.System.currentTimeMillis;

public class exFibonacci {

    public static void main(String[] args) {
        // test algo 1
        long b1 = 1, b2 = 2, b3 = 5, b4 = 10, b5 = 30, b6 = 1000000;
        long fb1[] = {fibo1(b1), fibo1(b2), fibo1(b3), fibo1(b4)};
        long fb2[] = {fibo2(b1), fibo2(b2), fibo2(b3), fibo2(b4)};
        long fb3[] = {fibo3(b1), fibo3(b2), fibo3(b3), fibo3(b4)};
        displayLongArray(fb1);
        displayLongArray(fb2);
        displayLongArray(fb3);

        testChronoFibo(1, b5);
        testChronoFibo(2, b6);
        testChronoFibo(3, b6);
    }

    public static void testChronoFibo(int algo, long m){
        long  s = currentTimeMillis();
        if( algo == 1)
            fibo1(m);
        else if ( algo == 2)
            fibo2(m);
        else
            fibo3(m);
        long e = currentTimeMillis();
        System.out.println("Time that algo"+ algo + " for entry( " + m + " ) take is :: " + (e - s) + " Milliseconds");
    }

    public static long fibo1(long f){
        if(f == 0)
            return 0;
        else if(f == 1 || f == 2)
            return 1;
        else
            return fibo1(f - 1) + fibo1( f - 2);
    }
    public static long fibo2(long f){
        long f1 = 0, f2 = 1;
        for (int i=0;i < f;i++){
            long tmp = f2;
            f2 = f1 + f2;
            f1 = tmp;
        }
        return f1;
    }

    public static long fibo3(long n){
        long i = 1, k = 0, j = 0, h = 1;
        while(n > 0){
            long tmp;
            if(n % 2 == 1){
                tmp = j * h;
                j = i * h + j * k + tmp;
                i = i * k + tmp;
            }
            tmp = h * h;
            h = 2 * k * h + tmp;
            k = k * k + tmp;
            n /= 2;
        }
        return j;
    }

    public static void displayLongArray(long [] t){
        for (long v : t)
            System.out.println("value : " + v);
    }
}
