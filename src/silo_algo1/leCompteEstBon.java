package silo_algo1;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static java.util.stream.Collectors.toList;

public class leCompteEstBon {

    public static void main(String[] args) {
        int test = 7;
        int[] listeEntiers1 = {1, 9, 2, 3};
        System.out.println("Liste 1 le compte est bon :: " + miniCompteEstBon(listeEntiers1, test));
        int[] listeEntiers2 = {2, 2, 4, 4, 5, 3, 9};
        System.out.println("Liste 2 le compte est bon :: " + miniCompteEstBon(listeEntiers2, test));
        int[] listeEntiers3 = {1, 8, 6, 3};
        System.out.println("Liste 3 le compte est bon :: " + miniCompteEstBon(listeEntiers3, test));
        int[] listeEntiers4 = {9, 9, 4, 2, 9, 7, 3};
        System.out.println("Liste 4 le compte est bon :: " + miniCompteEstBon(listeEntiers4, test));
        int[] listeEntiers5 = {2, 4, 5};
        System.out.println("Liste 1 le compte est bon :: " + miniCompteEstBon(listeEntiers5, test));
    }

    public static boolean miniCompteEstBon(int[] ls, int s){
        List<Integer> tmp = Arrays.stream(ls).boxed().collect(toList());
        int temp = 0, tempJump = 0;
        for (int i=0;i < ls.length;i++){
            for (int j = i; j < ls.length; j++) {
                temp += ls[j];
                if(j + 1 < ls.length)
                    tempJump =  ls[j + 1];
                if(!tmp.contains(temp))
                    tmp.add(temp);
                if(!tmp.contains(tempJump))
                    tmp.add(tempJump);
            }
        }
        System.out.println(tmp);
        if(tmp.contains(s))
            return true;
        return false;
    }

    public static List<Integer> compileArray(int[] ls, int idx){
        List<Integer> liste = new ArrayList<>();

        return liste;
    }

//    public static boolean countIsIn(int[] ls, int s, int idx){
//      if(idx >= ls.length)
//          return false;
//      else {
//          List<Integer> tmp = Arrays.stream(ls).boxed().collect(toList());
//          int temp = 0;
//          for (int i=0;i < ls.length;i++) {
//              temp += ls[i];
//              tmp.add(temp);
//          }
//          System.out.println("AT this step ::: " + tmp + " idx ::: " + idx);
//          if(tmp.contains(s))
//              return true;
//          else
//              countIsIn(ls, s, idx + 1);
//      }
//      return false;
//    }
//
//    public static boolean countIsIn(int[] ls, int s){
//        return countIsIn(ls, s, 0);
//    }
}
